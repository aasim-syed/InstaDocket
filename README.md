## Getting Started

1. Clone this repo.

2. Create a `.env.local` file with `BANANA_API_KEY=your_api_key` and `BANANA_MODEL_KEY=your_model_key`.

3. Install dependencies:

```bash
npm i
```

4. Run the development server:

```bash
npm run dev
```

5. Open [http://localhost:3000](http://localhost:3000) with your browser to see your project!